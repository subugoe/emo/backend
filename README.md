# Back end for the TIDO viewer

The back end with sample data for the released version of the TIDO Viewer.

## Summary

- [What It Is](#what-it-is)
- [Built With](#built-with)
- [Versioning](#versioning)
- [Authors](#authors)
- [License](#license)
- [Acknowledgments](#acknowledgments)

## What It Is

This repository contains a simple webpage that delivers JSON files according to the [SUB's TextAPI](https://subugoe.pages.gwdg.de/emo/text-api/) specification.
Its main collection, `sampledata/collection.json` (available at <https://subugoe.pages.gwdg.de/emo/backend/sampledata/collection.json>), serves as an entrypoint for the [TIDO viewer](https://gitlab.gwdg.de/subugoe/emo/tido).

Since all the JSON files comply to the TextAPI, TIDO is able to digest them and display the texts, images, and annotations.

## Built With

- [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/#gitlab-pages) - Used for serving the webpage
- [SUB TextAPI](https://subugoe.pages.gwdg.de/emo/text-api/) - Used for the text data
- [W3C Annotation Data Model](https://www.w3.org/TR/annotation-model/) - Used for the text's annotations

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions
available, see the [tags on this
repository](https://gitlab.gwdg.de/subugoe/emo/backend/-/tags).

## Authors

- **Michelle Weidling** - *initial work* -
    [mrodzis](https://gitlab.gwdg.de/mrodzis)

See also the list of
[contributors](https://gitlab.gwdg.de/subugoe/emo/backend/-/graphs/develop)
who participated in this project.

## License

This project is licensed under the [GNU Lesser General Public License v3.0](LICENSE.md).
See the [LICENSE.md](LICENSE.md) file for details.

## Acknowledgments

- [Mathias Göbel](https://gitlab.gwdg.de/mgoebel) for collating the intial examples that have been used in the "old" TIDO viewer and providing the idea for this solution based on GitLab Pages



