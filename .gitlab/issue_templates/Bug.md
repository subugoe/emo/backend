## Description

…

### I expected the following to happen

…

### On the contrary, I observed

…

## How to reproduce the bug

Please describe briefly how you discovered the bug and what a developer has to do to reproduce it.

_Steps:_

* Step 1
* Step 2
* ...

## Severity

How much impact does this bug have on the product or further development?

* [ ] Minor
* [ ] Major
* [ ] Critical
* [ ] Blocker

## Program and – if applicable – dependency version

Which version of the software did you use?

## Related Tickets

Add related issues.

/cc [Mathias Göbel](https://gitlab.gwdg.de/mgoebel), [Frank Schneider](https://gitlab.gwdg.de/schneider210), [Michelle Weidling](https://gitlab.gwdg.de/mrodzis)
