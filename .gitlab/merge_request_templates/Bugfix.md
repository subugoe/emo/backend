## Summary

…

## Does the result of the MR comply to our "definition of done"?

* [ ] Unit tests passed
* [ ] Code reviewed
* [ ] Acceptance criteria met
* [ ] Functional tests passed
* [ ] Non-Functional requirements met
* [ ] Product Owner accepts the User Story

### Related

### Closes

## Changelog

* [ ] I added a statement to the CHANGELOG.

## Version number

* [ ] I bumped the version number in `build.properties`.

/cc [Mathias Göbel](https://gitlab.gwdg.de/mgoebel), [Frank Schneider](https://gitlab.gwdg.de/schneider210), [Michelle Weidling](https://gitlab.gwdg.de/mrodzis)
