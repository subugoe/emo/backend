## Summary

This MR provides…

## Does the result of the MR comply to our "definition of done"?

* [ ] Unit tests passed
* [ ] Code reviewed
* [ ] Acceptance criteria met
* [ ] Functional tests passed
* [ ] Non-Functional requirements met
* [ ] Product Owner accepts the User Story

## Use Cases

If you can, please provide use cases for this feature.

## Documentation

Shall we add your feature to the documentation?

* [ ] I've already did it!
* [ ] At least I added a headline to the documentation.

### Function Documentation

* [ ] Of course I provided all my functions with an appropriate documentation.

### Are there parts of the documentation we have to adjust

* [ ] No.
* [ ] Yes, but I'd like someone else to do it.
* [ ] Yes, and I already did!

## Tests

Are we able to test this new feature?

* [ ] Yes, everything can be done via unit tests.
* [ ] Yes, you can test by following these steps: …
* [ ] No, it is not possible.

## Changelog

* [ ] I added a statement to the CHANGELOG.

## Version number

* [ ] I bumped the version number in `build.properties`.

## Related Tickets

Add all related issues and especially those to be closed.

### Related

### Closes

/cc [Mathias Göbel](https://gitlab.gwdg.de/mgoebel), [Frank Schneider](https://gitlab.gwdg.de/schneider210), [Michelle Weidling](https://gitlab.gwdg.de/mrodzis)
