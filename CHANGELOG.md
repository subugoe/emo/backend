# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.2.0] - 2021-05-28

### Bugfix

- Added missing root keys to annotationCollection and annotationPage

## [0.1.0] - 2020-08-28

### Added

- Initial commit and start of this CHANGELOG.
